'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

// Components
var Main = require('./App/android/Components/Main');

/**
 * Project component
 * @author Sergii Startsev
 */
class testproject extends Component {
  render() {
    return (
      <Main />
    );
  }
}

AppRegistry.registerComponent('testproject', () => testproject);
