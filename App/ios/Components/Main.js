'use strict';

import React, {
  ActivityIndicatorIOS,
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';

var Dimensions = require('Dimensions');
var moment = require('moment');

// Libraries
var Video = require('react-native-video').default;
var GiftedMessenger = require('react-native-gifted-messenger');

// Services
var MessageService = require('../../Services/Message');

// calculation video proportions
var videoHeight = Dimensions.get('window').width*9/16
var videoWidth = Dimensions.get('window').width;

/**
 * Main component 
 * @author  Sergii Startsev
 */
class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // player
      url: "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8",
      volume: 1,
      muted: false,
      paused: false,
      duration: 0.0,
      currentTime: 0.0,
      // messages
      messages: [],
      message: '',
    };
  }
  /**
   * Handling when component did mount
   */
  componentDidMount() {
    this.getMessages();
  }
  /**
   * Toggle pause button
   */
  audioControlPressed() {
    this.setState({
      paused: !this.state.paused
    })
  }
  /**
   * Render player component
   * @return {JSX}
   */
  renderPlayer() {
    return(
      <View style={{ backgroundColor: 'rgb(0, 0, 0)' }}>
        {this.state.url != null ?
          <TouchableHighlight
            onPress={() => this.audioControlPressed(true)}
            underlayColor='transparent'
            >
            <Video source={{uri: this.state.url}}
               paused={this.state.paused}
               volume={this.state.volume}
               muted={this.state.muted}
               repeat={true}
               onError={() => {AlertIOS.alert('Error occurred!')}}
               style={styles.videoStyles}
               ref="playerComponent" />
          </TouchableHighlight>: null }
          
      </View>
    )
  }
  /**
   * Getting messages handler
   * @return {void}
   */
  getMessages() {
    MessageService.getList()
      .then((list) => {
        var messages = [];

        for (let i = 0; i < list.length; i++) {
          messages.push({
            text: list[i].message, 
            date: moment(list[i].created_at),
            image: null,
            position: 'left'
          });
        }

        this.setState({
          messages: messages
        })
      })
  }
  /**
   * On sending message
   * @param  {String} message
   * @return {void}
   */
  onSend(message) {
    MessageService.save(message.text)
      .then((r) => {})

    this._GiftedMessenger.appendMessage({
      text: message.text,
      position: 'left', 
      image: null,
      date: new Date(),
    });
    this._GiftedMessenger.onChangeText('');
  }
  /**
   * Render messages area
   * @return {JSX}
   */
  renderMessages() {
    return (
      <GiftedMessenger
        ref={(c) => this._GiftedMessenger = c}

        autoFocus={false}

        messages={this.state.messages}
        maxHeight={Dimensions.get('window').height - videoHeight}
        placeholder="Please enter your message here"

        onCustomSend={this.onSend.bind(this)}

        styles={{
          bubbleLeft: {
            backgroundColor: '#e6e6eb',
            marginRight: 70,
          },
          bubbleRight: {
            backgroundColor: '#007aff',
            marginLeft: 70,
          },
          textInput: {
            alignSelf: 'center',
            height: 30,
            width: Dimensions.get('window').width - 75,
            backgroundColor: '#FFF',
            flex: 1,
            padding: 0,
            margin: 0,
            fontSize: 15,
          }
        }}
        style={{
          width: Dimensions.get('window').width
        }} />
    );
  }
  /**
   * Main render
   * @return {JSX}
   */
  render() {
    return (
      <View style={styles.container}>
        {this.renderPlayer()}

        {this.renderMessages()}
      </View>
    );
  }

}

/**
 * Styles object 
 * @type {StyleSheet}
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  videoStyles: {
    height: videoHeight,
    width: videoWidth,
  },
  messageField: {
    marginLeft: 15,
    height: 26,
    color: '#ccc',
    textAlign: 'center',
  },
});

module.exports = Main;
