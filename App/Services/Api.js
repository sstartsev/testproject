/**
 * Api service
 * @type {Object}
 * @author Sergii Startsev
 */
var Api = {
    BASE_URL: 'http://my.staging.demio.com/rest/testing/'
};

module.exports = Api;
