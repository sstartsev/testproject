'use strict';

// Services
var Api = require('./Api');

/**
 * Message Service class
 * @author Sergii Startsev
 */
class MessageService {
  /**
   * Get main show list
   * @param {Function} cb
   */
  getList() {
    return fetch(Api.BASE_URL + 'get-list')
      .then((response) => response.json())
      .then((messages) => {
        if (typeof (messages) != 'undefined' && messages.length > 0) {
          return messages;
        }

        return [];
      })
  };
  /**
   * Send message action
   * @param  {String} message 
   * @return {Promise}
   */
  save(message) {
    var body = [];
    body.push("message=" + encodeURIComponent(message));

    return fetch(Api.BASE_URL + 'save',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body.join("&")
      }
    ).then((r) => console.log(r))
  };
  /**
   * Delete message action
   * @param  {Number} id 
   * @return {Promise}
   */
  deleteMessage(id) {
    var body = [];
    body.push("id=" + encodeURIComponent(id));

    return fetch(Api.BASE_URL + 'delete',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: body.join("&")
      }
    ).then((r) => console.log(r))
  };

}

module.exports = new MessageService();